#undef FNAME
#undef NAME
#undef ICON

/* metadata */
#define FNAME poly_blend_start
#define NAME "Polygon Blend"
#define ICON "rect.png"

#ifndef PROTO
# ifndef UI
#  include "main.h"

/* standard var */
static int done = 0;
/* private data */
static Evas_Object *o_images[OBNUM];

static void
poly(Evas_Object *o, int type, Evas_Coord x, Evas_Coord y)
{
   efl_canvas_polygon_points_clear(o);
   switch (type % 4)
     {
      case 0: /* triangle */
         efl_canvas_polygon_point_add(o, EINA_POSITION2D(x + 50, y + 0));
         efl_canvas_polygon_point_add(o, EINA_POSITION2D(x + 100, y + 100));
         efl_canvas_polygon_point_add(o, EINA_POSITION2D(x + 0, y + 100));
         break;
      case 1: /* square */
         efl_canvas_polygon_point_add(o, EINA_POSITION2D(x + 0, y + 0));
         efl_canvas_polygon_point_add(o, EINA_POSITION2D(x + 100, y + 0));
         efl_canvas_polygon_point_add(o, EINA_POSITION2D(x + 100, y + 100));
         efl_canvas_polygon_point_add(o, EINA_POSITION2D(x + 0, y + 100));
         break;
      case 2: /* hex */
         efl_canvas_polygon_point_add(o, EINA_POSITION2D(x + 50, y + 0));
         efl_canvas_polygon_point_add(o, EINA_POSITION2D(x + 100, y + 30));
         efl_canvas_polygon_point_add(o, EINA_POSITION2D(x + 100, y + 70));
         efl_canvas_polygon_point_add(o, EINA_POSITION2D(x + 50, y + 100));
         efl_canvas_polygon_point_add(o, EINA_POSITION2D(x + 0, y + 70));
         efl_canvas_polygon_point_add(o, EINA_POSITION2D(x + 0, y + 30));
         break;
      case 3: /* star */
         efl_canvas_polygon_point_add(o, EINA_POSITION2D(x + 50, y + 0));
         efl_canvas_polygon_point_add(o, EINA_POSITION2D(x + 60, y + 40));
         efl_canvas_polygon_point_add(o, EINA_POSITION2D(x + 90, y + 30));
         efl_canvas_polygon_point_add(o, EINA_POSITION2D(x + 70, y + 60));
         efl_canvas_polygon_point_add(o, EINA_POSITION2D(x + 90, y + 100));
         efl_canvas_polygon_point_add(o, EINA_POSITION2D(x + 50, y + 70));
         efl_canvas_polygon_point_add(o, EINA_POSITION2D(x + 10, y + 100));
         efl_canvas_polygon_point_add(o, EINA_POSITION2D(x + 30, y + 60));
         efl_canvas_polygon_point_add(o, EINA_POSITION2D(x + 10, y + 30));
         efl_canvas_polygon_point_add(o, EINA_POSITION2D(x + 40, y + 40));
         break;
      default:
         break;
     }
}

/* setup */
static void _setup(void)
{
   int i;
   Evas_Object *o;
   srnd();
   for (i = 0; i < OBNUM; i++)
     {
	int r, g, b, a;

        o = efl_add(EFL_CANVAS_POLYGON_CLASS, evas);
	o_images[i] = o;
	a = (rnd()&0xff) / 2;
	r = ((rnd()&0xff) * a) / 255;
	g = ((rnd()&0xff) * a) / 255;
	b = ((rnd()&0xff) * a) / 255;
	efl_gfx_color_set(o, r, g, b, a);
	efl_gfx_entity_visible_set(o, EINA_TRUE);
	poly(o, i, 0, 0);
     }
   done = 0;
}

/* cleanup */
static void _cleanup(void)
{
   int i;
   for (i = 0; i < OBNUM; i++) efl_del(o_images[i]);
}

/* loop - do things */
static void _loop(double t, int f)
{
   int i;
   Evas_Coord x, y, w, h;
   Evas_Object *o;

   for (i = 0; i < OBNUM; i++)
     {
	o = o_images[i];
        if (o)
	exp_size_get(o, &w, &h);
	x = (win_w / 2) - (w / 2);
	x += sin((double)(f + (i * 13)) / (36.7 * SLOW)) * (win_w / 4);
	y = (win_h / 2) - (h / 2);
	y += cos((double)(f + (i * 28)) / (43.8 * SLOW)) * (win_h / 4);
	if (o)
	efl_gfx_entity_position_set(o, EINA_POSITION2D(x, y));
     }
   FPS_STD(NAME);
}

/* prepend special key handlers if interactive (before STD) */
static void _key(const char *key)
{
   KEY_STD;
}












/* template stuff - ignore */
# endif
#endif

#ifdef UI
_ui_menu_item_add(ICON, NAME, FNAME);
#endif

#ifdef PROTO
void FNAME(void);
#endif

#ifndef PROTO
# ifndef UI
void FNAME(void)
{
   ui_func_set(_key, _loop, _setup);
}
# endif
#endif
#undef FNAME
#undef NAME
#undef ICON

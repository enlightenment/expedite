#undef FNAME
#undef NAME
#undef ICON

/* metadata */
#define FNAME image_data_ycbcr601pl_map_solid_rotate_start
#define NAME "Image Data YCbCr 601 Pointer List Map Solid Rotate"
#define ICON "data.png"

#ifndef PROTO
# ifndef UI
#  include "main.h"

/* standard var */
static int done = 0;

/* private data */
static Evas_Object *o_images[1];
static Eina_Slice slice[3];

/* setup */
static void _setup(void)
{
   FILE *f;
   for (int i = 0; i < 1; i++)
     {
        Evas_Object *o = efl_add(EFL_CANVAS_IMAGE_CLASS, evas);
        o_images[i] = o;
        efl_gfx_image_content_hint_set(o, EVAS_IMAGE_CONTENT_HINT_DYNAMIC);
        efl_gfx_buffer_alpha_set(o, 0);
        efl_gfx_fill_set(o, EINA_RECT(0, 0, 640, 480));
        efl_gfx_entity_size_set(o, EINA_SIZE2D(640, 480));
        efl_gfx_entity_visible_set(o, EINA_TRUE);

        slice[0].len = 640 * 480;
        slice[1].len = 320 * 240;
        slice[2].len = 320 * 240;
        f = fopen(build_path("tp.yuv"), "rb");
        if (!f) continue;
        for (int p = 0; p < 3; p++)
          {
             slice[p].mem = malloc(slice[p].len);
             fread((void *) slice[p].mem, slice[p].len, 1, f);
             efl_gfx_buffer_managed_set(o, &slice[p], EINA_SIZE2D(640, 480), 0, EFL_GFX_COLORSPACE_YCBCR422P601_PL, p);
          }
        fclose(f);
     }
   done = 0;
}

/* cleanup */
static void _cleanup(void)
{
   for (int i = 0; i < 1; i++)
     {
        Evas_Object *o = o_images[i];
        for (int p = 0; p < 3; p++)
          {
             efl_gfx_buffer_managed_set(o, NULL, EINA_SIZE2D(640, 480), 0, EFL_GFX_COLORSPACE_YCBCR422P601_PL, p);
             free((void *) slice[p].mem);
          }
        efl_del(o);
     }
}

/* loop - do things */
static void _loop(double t, int f)
{
   int i;
   static Evas_Map *m = NULL;
   Evas_Coord x, y, w, h;
   if (!m) m = evas_map_new(4);
   for (i = 0; i < 1; i++)
     {
        w = 640;
	h = 480;
	x = (win_w / 2) - (w / 2);
	y = (win_h / 2) - (h / 2);

	evas_map_util_points_populate_from_geometry(m, x, y, w, h, 0);

        evas_map_util_rotate(m, f, x + (w / 2), y + (h / 2));

        evas_object_map_enable_set(o_images[i], 1);
        evas_object_map_set(o_images[i], m);
     }
   FPS_STD(NAME);
}

/* prepend special key handlers if interactive (before STD) */
static void _key(const char *key)
{
   KEY_STD;
}












/* template stuff - ignore */
# endif
#endif

#ifdef UI
_ui_menu_item_add(ICON, NAME, FNAME);
#endif

#ifdef PROTO
void FNAME(void);
#endif

#ifndef PROTO
# ifndef UI
void FNAME(void)
{
   ui_func_set(_key, _loop, _setup);
}
# endif
#endif
#undef FNAME
#undef NAME
#undef ICON

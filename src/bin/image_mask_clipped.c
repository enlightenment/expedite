#undef FNAME
#undef NAME
#undef ICON

/* metadata */
#define FNAME image_mask_clipped
#define NAME "Image Mask Clipped"
#define ICON "blend.png"

#ifndef PROTO
# ifndef UI
#  include "main.h"

/* standard var */
static int done = 0;
/* private data */
static Evas_Object *o_images[OBNUM];
static Evas_Object *o_mask;
static Evas_Object *o_clip;

/* setup */
static void _setup(void)
{
   int i;
   Evas_Object *o;

   o = efl_add(EFL_CANVAS_RECTANGLE_CLASS, evas);
   o_clip = o;
   efl_gfx_entity_size_set(o, EINA_SIZE2D(180, 105));
   efl_gfx_entity_position_set(o, EINA_POSITION2D((win_w - 180) / 2, (win_h - 105) / 2));
   efl_gfx_entity_visible_set(o, EINA_TRUE);

   o = efl_add(EFL_CANVAS_IMAGE_CLASS, evas);
   o_mask = o;
   efl_file_simple_load(o, build_path("e-logo-mask.png"), NULL);
   efl_gfx_fill_set(o, EINA_RECT(0, 0, 720, 420));
   efl_gfx_entity_size_set(o, EINA_SIZE2D(720, 420));
   evas_object_clip_set(o, o_clip);
   efl_canvas_object_has_fixed_size_set(o, 1);
   efl_gfx_entity_position_set(o, EINA_POSITION2D((win_w - 720) / 2, (win_h - 420) / 2));
   efl_gfx_entity_visible_set(o, EINA_TRUE);

   for (i = 0; i < OBNUM; i++)
     {
        o = efl_add(EFL_CANVAS_IMAGE_CLASS, evas);
        o_images[i] = o;
        efl_file_simple_load(o, build_path("logo.png"), NULL);
        efl_gfx_fill_set(o, EINA_RECT(0, 0, 360, 480));
        efl_gfx_entity_size_set(o, EINA_SIZE2D(360, 480));
        efl_gfx_image_smooth_scale_set(o, 0);
        evas_object_clip_set(o, o_mask);
        efl_gfx_entity_visible_set(o, EINA_TRUE);
     }
   done = 0;
}

/* cleanup */
static void _cleanup(void)
{
   int i;
   for (i = 0; i < OBNUM; i++) efl_del(o_images[i]);
   efl_del(o_mask);
   efl_del(o_clip);
}

/* loop - do things */
static void _loop(double t, int f)
{
   int i;
   Evas_Coord x, y, w, h, w0, h0;

   for (i = 0; i < OBNUM; i++)
     {
        w0 = 80;
        h0 = 80;
        w = 5 + ((1.0 + cos((double)(f + (i * 10)) / (7.4 * SLOW) )) * w0 * 2);
        h = 5 + ((1.0 + sin((double)(f + (i * 19)) / (12.6 * SLOW) )) * h0 * 2);
        x = (win_w / 2) - (w / 2);
        x += sin((double)(f + (i * 13)) / (36.7 * SLOW)) * (w0 / 2);
        y = (win_h / 2) - (h / 2);
        y += cos((double)(f + (i * 28)) / (43.8 * SLOW)) * (h0 / 2);

        efl_gfx_entity_position_set(o_images[i], EINA_POSITION2D(x, y));
        efl_gfx_entity_size_set(o_images[i], EINA_SIZE2D(w, h));
        efl_gfx_fill_set(o_images[i], EINA_RECT(0, 0, w, h));
     }
   w = (sin((double)f) * 720 + 840) / 2.2;
   h = (sin((double)f) * 720 + 840) / 2.2;
   efl_gfx_entity_size_set(o_mask, EINA_SIZE2D(w, h));
   efl_gfx_entity_position_set(o_mask, EINA_POSITION2D((win_w - w) / 2, (win_h - h) / 2));
   FPS_STD(NAME);
}

/* prepend special key handlers if interactive (before STD) */
static void _key(const char *key)
{
   KEY_STD;
}












/* template stuff - ignore */
# endif
#endif

#ifdef UI
_ui_menu_item_add(ICON, NAME, FNAME);
#endif

#ifdef PROTO
void FNAME(void);
#endif

#ifndef PROTO
# ifndef UI
void FNAME(void)
{
   ui_func_set(_key, _loop, _setup);
}
# endif
#endif
#undef FNAME
#undef NAME
#undef ICON

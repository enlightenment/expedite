#undef FNAME
#undef NAME
#undef ICON

/* metadata */
#define FNAME image_quality_scale_start
#define NAME "Image Quality Scale"
#define ICON "data.png"

#ifndef PROTO
# ifndef UI
#  include "main.h"

/* standard var */
static int done = 0;

/* private data */
static Evas_Object *o_images[1];

/* setup */
static void _setup(void)
{
   int i;
   Evas_Object *o;
   for (i = 0; i < 1; i++)
     {
	o = efl_add(EFL_CANVAS_IMAGE_CLASS, evas);
	o_images[i] = o;
	efl_file_simple_load(o, build_path("tp.png"), NULL);
	efl_gfx_fill_set(o, EINA_RECT(0, 0, 640, 480));
	efl_gfx_entity_size_set(o, EINA_SIZE2D(640, 480));
	efl_gfx_entity_visible_set(o, EINA_TRUE);
     }
   done = 0;
}

/* cleanup */
static void _cleanup(void)
{
   int i;
   for (i = 0; i < 1; i++) efl_del(o_images[i]);
}

/* loop - do things */
static void _loop(double t, int f)
{
   int i;
   Evas_Coord x, y, w, h;
   for (i = 0; i < 1; i++)
     {
        w = 640;
	h = 480;
	w *= (f / 100.0) * 4.0;
	h *= (f / 100.0) * 4.0;
	x = (win_w / 2) - (w / 2);
	y = (win_h / 2) - (h / 2);
	efl_gfx_entity_position_set(o_images[i], EINA_POSITION2D(x, y));
	efl_gfx_entity_size_set(o_images[i], EINA_SIZE2D(w, h));
	efl_gfx_fill_set(o_images[i], EINA_RECT(0, 0, w, h));
     }
   FPS_STD(NAME);
}

/* prepend special key handlers if interactive (before STD) */
static void _key(const char *key)
{
   KEY_STD;
}












/* template stuff - ignore */
# endif
#endif

#ifdef UI
_ui_menu_item_add(ICON, NAME, FNAME);
#endif

#ifdef PROTO
void FNAME(void);
#endif

#ifndef PROTO
# ifndef UI
void FNAME(void)
{
   ui_func_set(_key, _loop, _setup);
}
# endif
#endif
#undef FNAME
#undef NAME
#undef ICON

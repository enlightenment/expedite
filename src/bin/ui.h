#ifndef UI_H
#define UI_H

typedef struct _Menu_Item Menu_Item;
struct _Menu_Item
{
   Evas_Object *o_icon;
   char *icon;
   char *text;
   void (*func) (void);

   Eina_Bool test;
};

extern int async_render;
extern Eina_List *menu;
extern double weights[];

void ui_setup(unsigned int ui_w, unsigned int ui_h);
Eina_Bool ui_loop(void *data);
void ui_menu(void);
void ui_func_set(void (*kfunc) (const char *key), void (*lfunc) (double t, int f), void (*lsetup) (void));
void ui_fps(double fps);
void ui_num(int n);
void ui_all(void);

#endif

#undef FNAME
#undef NAME
#undef ICON

/* metadata */
#define FNAME proxy_textblock_start
#define NAME "Proxy Textblock"
#define ICON "text.png"

#ifndef PROTO
# ifndef UI
#  include "main.h"

#undef OBNUM
#define OBNUM 4

/* standard var */
static int done = 0;

/* private data */
static Evas_Object *o_texts[OBNUM];

//uncomment to test vs plain textblock render perf
//#define PROXY_CMP 1

/* setup */
static void _setup(void)
{
   int i;
   Evas_Object *o, *clip, *proxy;

   for (i = 0; i < OBNUM; i++)
     {
        o = efl_add(EFL_CANVAS_TEXTBLOCK_CLASS, evas);
        o_texts[i] = o;
        efl_text_font_family_set(o, "Vera-Bold");
        efl_text_font_size_set(o, 12);
        efl_text_wrap_set(o, EFL_TEXT_FORMAT_WRAP_WORD);
        efl_text_multiline_set(o, 1);
        efl_text_set(o,
          "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do "
          "eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut "
          "enim ad minim veniam, quis nostrud exercitation ullamco laboris "
          "nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in "
          "reprehenderit in voluptate velit esse cillum dolore eu fugiat "
          "nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in "
          "culpa qui officia deserunt mollit anim id est laborum."
        );
        efl_text_color_set(o, 0, 0, 0, 255);
        efl_gfx_entity_size_set(o, EINA_SIZE2D(480, 160));
        efl_gfx_entity_visible_set(o, EINA_TRUE);

        i++;
        clip = efl_add(EFL_CANVAS_RECTANGLE_CLASS, evas);
        o_texts[i] = clip;
        efl_canvas_object_clipper_set(o, clip);
#ifdef PROXY_CMP
        efl_gfx_entity_size_set(clip, EINA_SIZE2D(480, 160));
#else
        efl_gfx_entity_size_set(clip, EINA_SIZE2D(480, 80));
#endif
        efl_gfx_entity_visible_set(clip, EINA_TRUE);
        efl_canvas_object_has_fixed_size_set(o, 1);

        i++;
        proxy = efl_add(EFL_CANVAS_PROXY_CLASS, evas);
        o_texts[i] = proxy;
        efl_canvas_proxy_source_set(proxy, o);
        efl_canvas_proxy_source_clip_set(proxy, EINA_FALSE);
        /* only draw the lower half of the proxy object */
        efl_gfx_fill_set(proxy, EINA_RECT(0, 80, 480, 80));
        efl_gfx_entity_size_set(proxy, EINA_SIZE2D(480, 160));
#ifdef PROXY_CMP
        efl_gfx_entity_visible_set(proxy, EINA_FALSE);
#else
        efl_gfx_entity_visible_set(proxy, EINA_TRUE);
#endif

        i++;
        /* restrict the sampling of the source text to the lower half */
        clip = efl_add(EFL_CANVAS_RECTANGLE_CLASS, evas);
        o_texts[i] = clip;
        efl_canvas_object_clipper_set(proxy, clip);
        efl_gfx_entity_size_set(clip, EINA_SIZE2D(480, 80));
#ifdef PROXY_CMP
        efl_gfx_entity_visible_set(clip, EINA_FALSE);
#else
        efl_gfx_entity_visible_set(clip, EINA_TRUE);
        efl_canvas_object_has_fixed_size_set(proxy, 1);
#endif
     }
   done = 0;
}

/* cleanup */
static void _cleanup(void)
{
   int i;
   for (i = 0; i < OBNUM; i++) efl_del(o_texts[i]);
}

/* loop - do things */
static void _loop(double t, int f)
{
   static int k = 0;
   int i;
   Evas_Coord x, y, w, h;
   for (i = 0; i < OBNUM; i++)
     {
        w = 480;
        h = 160;
        x = (win_w / 2) - (w / 2);
        x += sin((double)(f + ((i + k) * 13)) / (36.7 * SLOW)) * (w / 2);
        y = (win_h / 2) - (h / 2);
        y += cos((double)(f + ((i + k) * 28)) / (43.8 * SLOW)) * (h / 2);
        efl_gfx_entity_position_set(o_texts[i], EINA_POSITION2D(x, y));
        i++;
        efl_gfx_entity_position_set(o_texts[i], EINA_POSITION2D(x, y));
        i++;
        efl_gfx_entity_position_set(o_texts[i], EINA_POSITION2D(x, y));
        i++;
        efl_gfx_entity_position_set(o_texts[i], EINA_POSITION2D(x, y + (h / 2)));
     }
   ++k;
   FPS_STD(NAME);
}

/* prepend special key handlers if interactive (before STD) */
static void _key(const char *key)
{
   KEY_STD;
}












/* template stuff - ignore */
# endif
#endif

#ifdef UI
_ui_menu_item_add(ICON, NAME, FNAME);
#endif

#ifdef PROTO
void FNAME(void);
#endif

#ifndef PROTO
# ifndef UI
void FNAME(void)
{
   ui_func_set(_key, _loop, _setup);
}
# endif
#endif
#undef FNAME
#undef NAME
#undef ICON

#undef FNAME
#undef NAME
#undef ICON

/* metadata */
#define FNAME image_blend_clipped_proxy_start
#define NAME "Image Blend Clipped Proxy"
#define ICON "blend.png"

#ifndef PROTO
# ifndef UI
#  include "main.h"

/* standard var */
static int done = 0;
/* private data */
static Evas_Object *o_images[OBNUM * 2];

/* setup */
static void _setup(void)
{
   int i;
   Evas_Object *o,*src;

   o = efl_add(EFL_CANVAS_IMAGE_CLASS, evas);
   efl_file_simple_load(o, build_path("logo.png"), NULL);
   efl_gfx_fill_set(o, EINA_RECT(0, 0, 120, 160));
   efl_gfx_entity_size_set(o, EINA_SIZE2D(120, 160));
   efl_gfx_entity_visible_set(o, EINA_TRUE);
   src = o;
   o_images[0] = src;

   for (i = 1; i < OBNUM; i += 2)
     {
        Eo *clip;
        o = efl_add(EFL_CANVAS_PROXY_CLASS, evas);
        o_images[i] = o;
        efl_canvas_proxy_source_set(o, src);
        efl_gfx_entity_size_set(o, EINA_SIZE2D(120, 160));
        efl_gfx_fill_set(o, EINA_RECT(0, 0, 120, 160));
        efl_gfx_entity_visible_set(o, EINA_TRUE);

        clip = efl_add(EFL_CANVAS_RECTANGLE_CLASS, evas);
        o_images[i + 1] = clip;
        efl_canvas_object_clipper_set(o, clip);
        efl_gfx_entity_size_set(clip, EINA_SIZE2D(120, 80));
        efl_canvas_object_has_fixed_size_set(o, 1);
        efl_gfx_entity_visible_set(clip, EINA_TRUE);
     }
   done = 0;
}

/* cleanup */
static void _cleanup(void)
{
   int i;
   for (i = 0; i < OBNUM * 2; i++) efl_del(o_images[i]);
}

/* loop - do things */
static void _loop(double t, int f)
{
   int i;
   Evas_Coord x, y, w, h;
   for (i = 0; i < OBNUM * 2; i++)
     {
        w = 120;
        h = 160;
        x = (win_w / 2) - (w / 2);
        x += sin((double)(f + (i * 13)) / (36.7 * SLOW)) * (w / 2);
        y = (win_h / 2) - (h / 2);
        y += cos((double)(f + (i * 28)) / (43.8 * SLOW)) * (h / 2);
        efl_gfx_entity_position_set(o_images[i], EINA_POSITION2D(x, y));
        if (i < 1) continue;
        i++;
        if ((i - 1) % 4)
          efl_gfx_entity_position_set(o_images[i], EINA_POSITION2D(x, y + h / 2));
        else
          efl_gfx_entity_position_set(o_images[i], EINA_POSITION2D(x, y));
     }
   FPS_STD(NAME);
}

/* prepend special key handlers if interactive (before STD) */
static void _key(const char *key)
{
   KEY_STD;
}












/* template stuff - ignore */
# endif
#endif

#ifdef UI
_ui_menu_item_add(ICON, NAME, FNAME);
#endif

#ifdef PROTO
void FNAME(void);
#endif

#ifndef PROTO
# ifndef UI
void FNAME(void)
{
   ui_func_set(_key, _loop, _setup);
}
# endif
#endif
#undef FNAME
#undef NAME
#undef ICON


#undef FNAME
#undef NAME
#undef ICON

/* metadata */
#define FNAME image_blend_many_smooth_down_scaled_start
#define NAME "Image Blend Many Smooth Down Scaled"
#define ICON "blend.png"

#ifndef PROTO
# ifndef UI
#  include "main.h"

#define MANYNUM 8192

/* standard var */
static int done = 0;
/* private data */
static Evas_Object *o_images[MANYNUM];

/* setup */
static void _setup(void)
{
   int i, w, h, n;
   Evas_Object *o;
   for (i = 0; i < MANYNUM; i++)
     {
        o = efl_add(EFL_CANVAS_IMAGE_CLASS, evas);
        o_images[i] = o;
        n = rnd() % 100;
        w = 3 + ((n * (60 - 3)) / 100);
        h = 4 + ((n * (80 - 4)) / 100);
        efl_file_simple_load(o, build_path("logo.png"), NULL);
        efl_gfx_image_smooth_scale_set(o, 1);
        efl_gfx_entity_size_set(o, EINA_SIZE2D(w, h));
        efl_gfx_fill_set(o, EINA_RECT(0, 0, w, h));
        efl_gfx_entity_visible_set(o, EINA_TRUE);
     }
   done = 0;
}

/* cleanup */
static void _cleanup(void)
{
   int i;
   for (i = 0; i < MANYNUM; i++) efl_del(o_images[i]);
}

/* loop - do things */
static void _loop(double t, int f)
{
   int i;
   Evas_Coord x, y, w, h;
   for (i = 0; i < MANYNUM; i++)
     {
        exp_size_get(o_images[i], &w, &h);
        x = (win_w / 2) - (w / 2);
        x += sin((double)(f + (i * 13)) / (36.7 * SLOW)) * (win_w / 2);
        y = (win_h / 2) - (h / 2);
        y += cos((double)(f + (i * 28)) / (43.8 * SLOW)) * (win_h / 2);
        efl_gfx_entity_position_set(o_images[i], EINA_POSITION2D(x, y));
     }
   FPS_STD(NAME);
}

/* prepend special key handlers if interactive (before STD) */
static void _key(const char *key)
{
   KEY_STD;
}












/* template stuff - ignore */
# endif
#endif

#ifdef UI
_ui_menu_item_add(ICON, NAME, FNAME);
#endif

#ifdef PROTO
void FNAME(void);
#endif

#ifndef PROTO
# ifndef UI
void FNAME(void)
{
   ui_func_set(_key, _loop, _setup);
}
# endif
#endif
#undef FNAME
#undef NAME
#undef ICON

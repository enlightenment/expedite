#undef FNAME
#undef NAME
#undef ICON

/* metadata */
#define FNAME vg_basic_circle_start
#define NAME "VG Basic Circle"
#define ICON "vector.png"

#ifndef PROTO
# ifndef UI
#  include "main.h"

/* standard var */
static int done = 0;

/* private data */
static Eo *o_shapes[OBNUM];

/* setup
 * Creating Evas Objects, each holds a vector shape.
 * Then start moving these Evas Objects. */
static void _setup(void)
{
   unsigned int i;

   for (i = 0; i < OBNUM; i++)
     {
        Efl_VG *circle;
        Eo *vector;
        double r = 35, stroke_w = 3;

        vector = efl_add(EFL_CANVAS_VG_OBJECT_CLASS, evas);
        o_shapes[i] = vector;
        efl_gfx_entity_size_set(vector, EINA_SIZE2D(r * 2 + stroke_w * 2, r * 2 + stroke_w * 2));
        efl_gfx_entity_position_set(vector, EINA_POSITION2D(0, 0));
        efl_gfx_entity_visible_set(vector, EINA_TRUE);

        circle = efl_add(EFL_CANVAS_VG_SHAPE_CLASS, vector);
        efl_gfx_path_append_circle(circle, r + stroke_w, r + stroke_w, r);
        efl_gfx_shape_stroke_width_set(circle, stroke_w);
        efl_gfx_shape_stroke_color_set(circle, 128, 0, 128, 128);
        efl_gfx_shape_stroke_join_set(circle, EFL_GFX_JOIN_ROUND);

        efl_canvas_vg_object_root_node_set(vector, circle);
     }
   done = 0;
}

/* cleanup */
static void _cleanup(void)
{
   unsigned int i;

   for (i = 0; i < OBNUM; i++) efl_del(o_shapes[i]);
}

/* loop - do things */
static void _loop(double t, int f)
{
   int i;
   Evas_Coord x, y, w = 200, h = 200;
   for (i = 0; i < OBNUM; i++)
     {
        x = (win_w / 2) - (w / 2);
        x += sin((double)(f + (i * 13)) / (36.7 * SLOW)) * (w / 2);
        y = (win_h / 2) - (h / 2);
        y += cos((double)(f + (i * 28)) / (43.8 * SLOW)) * (w / 2);
        efl_gfx_entity_position_set(o_shapes[i], EINA_POSITION2D(x, y));
     }
   FPS_STD(NAME);
}

/* prepend special key handlers if interactive (before STD) */
static void _key(const char *key)
{
   KEY_STD;
}












/* template stuff - ignore */
# endif
#endif

#ifdef UI
_ui_menu_item_add(ICON, NAME, FNAME);
#endif

#ifdef PROTO
void FNAME(void);
#endif

#ifndef PROTO
# ifndef UI
void FNAME(void)
{
   ui_func_set(_key, _loop, _setup);
}
# endif
#endif
#undef FNAME
#undef NAME
#undef ICON

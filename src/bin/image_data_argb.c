#undef FNAME
#undef NAME
#undef ICON

/* metadata */
#define FNAME image_data_argb_start
#define NAME "Image Data ARGB"
#define ICON "data.png"

#ifndef PROTO
# ifndef UI
#  include "main.h"

/* standard var */
static int done = 0;

/* private data */
static Evas_Object *o_images[1];

/* setup */
static void _setup(void)
{
   Evas_Object *o;
   Eina_Slice sl;

   sl.len = 640 * 480 * 4;
   sl.mem = malloc(sl.len);
   for (int i = 0; i < 1; i++)
     {
        o = efl_add(EFL_CANVAS_IMAGE_CLASS, evas);
        o_images[i] = o;
        efl_gfx_image_content_hint_set(o, EVAS_IMAGE_CONTENT_HINT_DYNAMIC);
        efl_gfx_buffer_alpha_set(o, 0);
        efl_gfx_fill_set(o, EINA_RECT(0, 0, 640, 480));
        efl_gfx_entity_size_set(o, EINA_SIZE2D(640, 480));
        efl_gfx_entity_visible_set(o, EINA_TRUE);
        efl_gfx_buffer_managed_set(o, &sl, EINA_SIZE2D(640, 480), 0, EFL_GFX_COLORSPACE_ARGB8888, 0);
     }
   done = 0;
}

/* cleanup */
static void _cleanup(void)
{
   for (int i = 0; i < 1; i++)
     {
        Evas_Object *o = o_images[i];
        Eina_Slice sl = {};

        sl = efl_gfx_buffer_managed_get(o, 0);
        free((void *) sl.mem);
        efl_del(o);
     }
}

/* loop - do things */
static void _loop(double t, int f)
{
   for (int i = 0; i < 1; i++)
     {
        Evas_Object *o = o_images[i];
        unsigned int *data, *p;
        Evas_Coord x, y, w, h;
        Eina_Rw_Slice sl = {};
        int st;

        w = 640;
        h = 480;
        x = (win_w / 2) - (w / 2);
        y = (win_h / 2) - (h / 2);
        efl_gfx_entity_position_set(o, EINA_POSITION2D(x, y));
        efl_gfx_entity_size_set(o, EINA_SIZE2D(w, h));
        efl_gfx_fill_set(o, EINA_RECT(0, 0, w, h));
        sl = efl_gfx_buffer_map(o, EFL_GFX_BUFFER_ACCESS_MODE_WRITE,
                           &EINA_RECT(0, 0, w, h), EFL_GFX_COLORSPACE_ARGB8888, 0, &st);
        data = sl.mem;
        if (!data)
          {
             fprintf(stderr, "ERROR: Failed to map image!\n");
             continue;
          }
        st = st >> 2;
        p = sl.mem;
        for (y = 0; y < h; y++)
          {
             for (x = 0; x < w; x++)
               {
                  *p = ((((x * y) + f) << 8) ^ (x - y - f)) | 0xff000000;
                  p++;
               }
             p += (st - w);
          }
        efl_gfx_buffer_unmap(o, sl);
        efl_gfx_buffer_update_add(o, &EINA_RECT(0, 0, w, h));
     }
   FPS_STD(NAME);
}

/* prepend special key handlers if interactive (before STD) */
static void _key(const char *key)
{
   KEY_STD;
}












/* template stuff - ignore */
# endif
#endif

#ifdef UI
_ui_menu_item_add(ICON, NAME, FNAME);
#endif

#ifdef PROTO
void FNAME(void);
#endif

#ifndef PROTO
# ifndef UI
void FNAME(void)
{
   ui_func_set(_key, _loop, _setup);
}
# endif
#endif
#undef FNAME
#undef NAME
#undef ICON
